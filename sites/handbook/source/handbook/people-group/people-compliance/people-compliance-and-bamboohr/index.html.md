---
layout: handbook-page-toc
title: "People Compliance and BambooHR"
description: "The People Compliance team ensures compliance in the all team member-related document storage and retention in BambooHR."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
